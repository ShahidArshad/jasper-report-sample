package com.arshad.microservices.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.arshad.microservices.entity.Employee;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;


@Service
public class ReportService {
	
	public String export(String format) throws FileNotFoundException, JRException {
		//File file = ResourceUtils.getFile("classpath:employee.jrxml");
		File file = ResourceUtils.getFile("classpath:employee.jasper");
		//JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
		JasperReport jasperReport = (JasperReport)JRLoader.loadObjectFromFile(file.getAbsolutePath());

		 List<Employee> employees = List.of(new Employee("Arshad", 100000,1),new Employee("Ahamed", 100000,2));
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(employees);
		
		Map<String, Object> params = new HashMap<>();
		params.put("createdBy", "Arshad");
		JasperPrint jp = JasperFillManager.fillReport(jasperReport, params,ds);
		if(format.equalsIgnoreCase("html")) {
			JasperExportManager.exportReportToHtmlFile(jp,"D:\\test\\employee.html");
		}else {
			JasperExportManager.exportReportToPdfFile(jp,"D:\\test\\employee.pdf");
		}
		
		return "report generated inside D:\\\\test";
	}

}
