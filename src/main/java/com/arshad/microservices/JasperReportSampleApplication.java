package com.arshad.microservices;

import java.io.FileNotFoundException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.arshad.microservices.entity.Employee;
import com.arshad.microservices.service.ReportService;

import net.sf.jasperreports.engine.JRException;

@SpringBootApplication
@RestController
public class JasperReportSampleApplication {
	
	@Autowired
	private ReportService reportService;

	public static void main(String[] args) {
		SpringApplication.run(JasperReportSampleApplication.class, args);
	}
	
	@GetMapping("/employees")
	public List<Employee> getEmployees(){
		
		return List.of(new Employee("Arshad",1, 100000),new Employee("Ahamed",2, 100000));
		
	}
	
	
	
	@GetMapping("/report/{format}")
	public String report(@PathVariable String format ) throws FileNotFoundException, JRException {
		return reportService.export(format);
	}

}
